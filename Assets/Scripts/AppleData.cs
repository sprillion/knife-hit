﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Apple/Chance Apple", fileName = "New Apple")]
public class AppleData : ScriptableObject
{
    [Header("Шанс %"), Range(0f, 100f)]
    public float chanсeApple;
}
