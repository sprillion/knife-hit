﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Wood : MonoBehaviour
{
    public GameManager game;

    void FixedUpdate()
    {

        if (Math.Abs(game.randSpeed - game.woodSpeed) < 0.01f)
        {
            game.randSpeed = Random.Range(-5f, 5f);
        }

        game.woodSpeed = Mathf.Lerp(game.woodSpeed, game.randSpeed, Time.deltaTime * 1.5f);

        if (!game.stopWood)
            transform.Rotate(0, 0, game.woodSpeed);

    }
}
