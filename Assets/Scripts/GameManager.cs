﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public int allScore ;
    public int currentScore;

    public Text textCurrentScore;
    public Text textLevelGame;
    public Text textRecordGame;
    public Text textRecordMenu;

    public GameObject endGamePanel;
    public GameObject buttonPlay;

    public int levelGame = 1;
    [NonSerialized]
    public List<int> countKnife = new List<int>()
    {
        0, 4, 5, 6, 7, 9, 5, 7, 8, 10, 12, 6, 5, 12, 13, 15
    };

    public Image[] iconsKnives;

    public float speedKnife;

    public List<Knife> knivesScripts;

    public GameObject[] randomKnives;
    //[NonSerialized]
    public int currentKnife = 0;

    public GameObject currentKnifeObj;
    
    public GameObject allKnives;

    public GameObject wood;
    [Range(-5f, 5f)]
    public float woodSpeed;

    public float randSpeed;
    public bool stopWood;

    [SerializeField]
    private Touch touchCollider;

    public Animator knifeAnimation;
    public Animator woodAnimation;

    public GameObject brokenWoodAnimation;

    [SerializeField]
    private AppleData appleData;
    [SerializeField]
    private GameObject appleObj;
    private void Start()
    {
        Vibration.Init();
        if (PlayerPrefs.HasKey("Score"))
        {
            allScore = PlayerPrefs.GetInt("Score");
        }
        else
        {
            allScore = 0;
        }

        if (PlayerPrefs.HasKey("MaxLevel"))
        {
            textRecordMenu.text = "Рекорд:\n" + PlayerPrefs.GetInt("MaxLevel") + " уровень";
        }

        textCurrentScore.text = allScore + "";

        randSpeed = woodSpeed;
    }

    public void Touch()
    {
        if (currentKnife < countKnife[levelGame])
        {
            knivesScripts[currentKnife].transform.parent = null;
            knivesScripts[currentKnife].rb.AddForce(Vector2.up * speedKnife, ForceMode2D.Impulse);
            iconsKnives[currentKnife].color = Color.gray;
            currentKnife++;

            if (currentKnife == countKnife[levelGame])
            {
                currentKnife = 0;
            }
        }

    }

    public void StartGame()
    {
        currentScore = 0;
        levelGame = 1;
        textCurrentScore.transform.parent.gameObject.SetActive(true);
        textCurrentScore.text = "0";
        textRecordMenu.gameObject.SetActive(false);
        textLevelGame.text = "Уровень " + levelGame;
        StartCoroutine(DeleyTouch());
        wood.SetActive(true);
        knifeAnimation.SetBool("Start", true);
        ShowIcons();
        woodAnimation.Play("StartGame");
        ShowApple();
        RandomKnives();
        stopWood = false;
        currentKnifeObj = knivesScripts[0].gameObject;
    }

    private IEnumerator DeleyTouch()
    {
        yield return new WaitForSeconds(1f);
        knifeAnimation.SetBool("Start", false);
        knifeAnimation.enabled = false;
        touchCollider.gameObject.SetActive(true);
    }

    public void NextLevel()
    {


        levelGame++;

        if (levelGame >= countKnife.Count)
        {
            Win();
            return;
        }
        KnivesBack();
        StartCoroutine(NextLevelCoroutine());
    }

    public IEnumerator NextLevelCoroutine()
    {
        appleObj.SetActive(false);
        touchCollider.gameObject.SetActive(false);
        touchCollider.delayTouch = false;
        wood.SetActive(false);
        brokenWoodAnimation.SetActive(true);
        yield return new WaitForSeconds(0.8f);
        brokenWoodAnimation.SetActive(false);
        wood.SetActive(true);
        touchCollider.gameObject.SetActive(true);

        ShowApple();
        RandomKnives();


        ShowIcons();

        textLevelGame.text = "Уровень " + levelGame;
        if (levelGame % 5 == 0)
            textLevelGame.text += "\n БОСС";
    }

    public void Win()
    {
        currentKnife = 0;
        textRecordGame.gameObject.SetActive(false);
        touchCollider.gameObject.SetActive(false);
        touchCollider.delayTouch = false;

        Debug.Log("Победа");
        Vibration.VibrateNope();
        allScore += currentScore;
        PlayerPrefs.SetInt("Score", allScore);
        SaveRecord();
        levelGame = 0;
        textLevelGame.text = "";
        stopWood = true;
        wood.SetActive(false);
        endGamePanel.SetActive(true);
        endGamePanel.GetComponentsInChildren<Text>()[0].text = "ПОБЕДА!";
    }

    private void KnivesBack()
    {
        foreach (var e in knivesScripts)
        {
            e.gameObject.SetActive(false);
            e.transform.parent = allKnives.transform;
        }
    }

    private void ShowApple()
    {
        var rand = Random.value;
        Debug.Log(rand);
        if (rand < appleData.chanсeApple/100)
        {
            appleObj.SetActive(true);
        }
    }

    private void RandomKnives()
    {

        foreach (var e in randomKnives)
        {
            e.SetActive(false);
        }

        var count = Random.Range(1, 4);

        for (int i = 0; i < count;)
        {
            var j = Random.Range(0, randomKnives.Length);
            if (randomKnives[j].activeSelf)
                continue;
            randomKnives[j].SetActive(true);
            i++;
        }

    }
    private void ShowIcons()
    {
        for (int i = 0; i < iconsKnives.Length; i++)
        {
            iconsKnives[i].gameObject.SetActive(i < countKnife[levelGame]);
            iconsKnives[i].color = Color.blue;
        }
    }

    public void EndGame()
    {
        StartCoroutine(EndGameCoroutine());
    }

    public IEnumerator EndGameCoroutine()
    {
        currentKnife = 0;
        textRecordGame.gameObject.SetActive(false);
        touchCollider.gameObject.SetActive(false);
        touchCollider.delayTouch = false;
        knifeAnimation = currentKnifeObj.GetComponent<Animator>();
        knifeAnimation.enabled = true;
        knifeAnimation.SetBool("Start", false);
        knifeAnimation.SetBool("End", true);
        Debug.Log("Промазал");

        allScore += currentScore;
        PlayerPrefs.SetInt("Score", allScore);
        SaveRecord();
        levelGame = 0;
        textLevelGame.text = "";
        yield return new WaitForSeconds(0.5f);
        stopWood = true;
        wood.SetActive(false);
        endGamePanel.SetActive(true);
        endGamePanel.GetComponentsInChildren<Text>()[0].text = "КОНЕЦ ИГРЫ";
    }

    private void SaveRecord()
    {
        if (PlayerPrefs.HasKey("MaxLevel"))
        {
            if (levelGame <= PlayerPrefs.GetInt("MaxLevel"))
            {
                return;
            }
        }
        textRecordGame.gameObject.SetActive(true);
        textRecordGame.text = "У ВАС НОВЫЙ РЕКОРД!\n" + levelGame + " УРОВЕНЬ";
        PlayerPrefs.SetInt("MaxLevel", levelGame);

    }

    public void KnifeToNull(Knife kn, float y)
    {
        kn.entered = false;
        kn.inWood = false;
        kn.transform.position = new Vector3(0, y, 0);
        kn.transform.rotation = new Quaternion();
        kn.enabled = true;
        kn.gameObject.SetActive(true);
        currentKnifeObj = kn.gameObject;
    }


    public void GoToMenu()
    {
        knifeAnimation.SetBool("End", false);
        KnivesBack();
        ShowIcons();
        knivesScripts[0].transform.parent = null;
        knivesScripts[0].enabled = true;
        knifeAnimation.enabled = false;
        knifeAnimation = knivesScripts[0].GetComponent<Animator>();
        knifeAnimation.enabled = true;
        KnifeToNull(knivesScripts[0], 0.5f);
        buttonPlay.SetActive(true);

        textRecordMenu.gameObject.SetActive(true);
        textRecordMenu.text = "Рекорд:\n" + PlayerPrefs.GetInt("MaxLevel") + " уровень";
        textCurrentScore.text = allScore + "";
    }

}
