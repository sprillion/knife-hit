﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Touch : MonoBehaviour
{
    [SerializeField]
    private GameManager game;

    public bool delayTouch;

    private IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.1f);
        delayTouch = false;
    }

    private void OnMouseDown()
    {
        if (!delayTouch)
        {
            game.Touch();

            delayTouch = true;
            StartCoroutine("Delay");
        }
    }
}
