﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class Knife : MonoBehaviour
{
    [SerializeField]
    private GameManager game;

    public bool inWood;

    public float startY;

    public Rigidbody2D rb;

    [NonSerialized]
    public bool entered;
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (entered)
            return;

        if (col.gameObject.tag == "Knife")
        {
            entered = true;
            rb.velocity = Vector2.zero;
            Vibration.VibratePeek();
            game.EndGame();
        }

        if (col.gameObject.tag == "Wood")
        {
            entered = true;
            rb.velocity = Vector2.zero;
            inWood = true;
            transform.parent = col.transform;
            Vibration.VibratePop();
            if (game.currentKnife == 0)
                game.NextLevel();
            game.KnifeToNull(game.knivesScripts[game.currentKnife], startY);
            enabled = false;
        }

        if (col.gameObject.tag == "Apple")
        {
            game.currentScore++;
            game.textCurrentScore.text = game.currentScore + "";
            col.gameObject.SetActive(false);
        }
    }
}
